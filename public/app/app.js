'use strict';

let { BrowserRouter, Match, Link } = ReactRouter;

var CurrencyBox = React.createClass({
  loadFromServer: function() {
    var date = moment();
    var hour = date.format("HH");
    var subtractDay = (hour >= 0 && hour <= 8) ? 2 : 1;
    var yesterday = date.subtract(subtractDay, 'days').format("YYYY-MM-DD");
    var latestUrl = this.props.url + '/latest?base=USD';
    var yesterdayUrl = this.props.url + '/' + yesterday + '?base=USD';

    $.ajax({
      url: latestUrl,
      dataType: 'json',
      cache: false,
      success: data => {
        this.setState({data: data});
      },
      error: (xhr, status, err) => {
        console.error(latestUrl, status, err.toString());
      }
    });

    $.ajax({
      url: yesterdayUrl,
      dataType: 'json',
      cache: false,
      success: data => {
        this.setState({ yesterdayData: data });
      },
      error: (xhr, status, err) => {
        console.error(yesterdayUrl, status, err.toString());
      }
    });
  },
  getInitialState: function() {
    return {
      data: {rates: {}},
      yesterdayData: {rates: {}}
    };
  },
  componentDidMount: function() {
    this.loadFromServer();
  },
  render: function() {
    let data = {
      today: this.state.data.rates,
      yesterday: this.state.yesterdayData.rates
    }

    return (
      <div className="quotesBox">
        <div className="ui header">
        </div>
        <div className="ui container">
          <CurrencyList rates={data} />
        </div>
      </div>
    );
  }
});

var CurrencyList = React.createClass({
  render: function() {
    var rateNodes = [];
    var rates = this.props.rates.today;
    var yesterdayRates = this.props.rates.yesterday;
    for (var currency in rates) {
      // what if today has a currency and yesterday doesn't? Is that possible?
      if (rates.hasOwnProperty(currency)) {
        rateNodes.push(
          <Currency key={currency} currency={currency} rate={rates[currency]} yesterdayRate={yesterdayRates[currency]}>
            {rates[currency]}
          </Currency>
        );
      }
    }
    return (
      <div className="quoteList ui three stackable cards">
        {rateNodes}
      </div>
    );
  }
});

var Currency = React.createClass({
  render: function() {
    var color = " ";
    var movement = "";
    var diffRate = this.props.rate - this.props.yesterdayRate;
    var percentGain = ((diffRate / this.props.yesterdayRate) * 100).toFixed(2);
    if (percentGain > 0) {
      percentGain = "+" + percentGain;
      color = " green ";
      movement = "caret up ";
    } else if (percentGain < 0) {
      color = " red ";
      movement = "caret down ";
    }
    var valueClass = "ui right floated mini" + color + "statistic";
    var movementClass = movement + "icon";

    var link = "/" + this.props.currency;

    return (
      <Link className = "quote ui link card" to={link}>
        <div className = "content">
          <div className = "description">

            <span className = "middle aligned code" >{this.props.currency}</span>
            <span className = {valueClass}>
              <div className = "value align-">
                <i className = { movementClass }></i>
                { this.props.rate }
              </div>
              <div class="label">
                <span className = {valueClass} >{percentGain}%</span>

              </div>
            </span>

          </div>
        </div>
      </Link>
    );
  }
});

var CurrencyDetail = React.createClass({
  render: function() {
    return (
      <div className="ui container">
        <div className="ui header">
          <Link className="" to="/">
            Back
          </Link>
        </div>
        <div className="ui container">
          <div className="ui fluid card">
            <div className="content">
              <div className="center aligned header">
                {this.props.params.currency}
              </div>
              <div className="meta">
                <span className="start date"></span>
                <span className="end date"></span>
              </div>
              <div className="description">
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

var App = React.createClass({
  render: function() {
    return (
      <CurrencyBox url="https://api.fixer.io" />
    );
  }
});

ReactDOM.render(
  <BrowserRouter>
    <Match exactly pattern="/" component={App} />
    <Match pattern="/:currency" component={CurrencyDetail} />
  </BrowserRouter>,
  document.getElementById('content')
);
